package com.company;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Date;

public class Orders {
    LinkedList<Order> orders;
    HashMap<Date, Order> hashMap;

    Orders(){
        orders = new LinkedList<>();
        hashMap = new HashMap<>();
    }

    public void add(Order order){
        orders.add(order);
        hashMap.put(order.TimeOfCreate, order);
    }

    public void show(){
        System.out.println("\n");
        if (orders.isEmpty()) {
            System.out.println("Список закаков пуст");
        } else {
            for (int i = 0; i < orders.size(); i++) {
                orders.get(i).show();
            }
        }
    }

    public void review(){
        String a = "Обработан";
        for (int i = 0; i < orders.size(); i++) {
            if (orders.get(i).getStatus().toString().equals(a)) {
                orders.remove(i);
            }
        }
    }

}
