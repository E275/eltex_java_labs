package com.check;

import com.company.*;

import java.util.concurrent.TimeUnit;

public class Generator extends ACheck {

    private int count; //кол-во товаров в заказе

    public Generator(){
        super();
        count = 1;
    }

    public Generator(Orders<Order> orders){
        super(orders);
        count = 1;
    }

    public Thread getThread(){
        return t;
    }

    public Order generate(){
        ShoppingCart<Clothes> Cart = new ShoppingCart<>();
        Order order = new Order();
        Clothes tshirt = new TShirt();
        Clothes cap = new Cap();

        int rand = (int)(Math.random() * 2);
        switch(rand){
            case 0:
                tshirt.create();
                Cart.add(tshirt);
                break;

            case 1:
                cap.create();
                Cart.add(cap);
                break;

            default:
                break;
        }

        Credentials man = new Credentials();
        man.create();

        order.checkout(Cart, man);
        return order;

    }

    public int getCount(){
       return count;
    }

    public void setCount(int a){
        count = a;
    }


    @Override
    public void run() {
        synchronized (orders) {
            for (int i = 0; i < count; i++)
                orders.add(generate());
            }
    }

}
