package com.company;

import java.util.UUID;

public class TShirt extends Clothes{

    int Size;
    StringBuilder Color = new StringBuilder();

    public TShirt(){
        super();
    }

    public TShirt(UUID _id){
        super(_id);
    }

    @Override
    public void create() {
        super.create();

        Size = (int)(Math.random() * 35 + 1);
        Color.append(ColorArray[(int)(Math.random() * 4)]);
    }

    @Override
    public void read() {
        System.out.print("Class: TShirt");

        super.read();

        System.out.println("Size: " + Size);
        System.out.println("Color: " + Color);
        System.out.println();
    }

    @Override
    public void update() {
        super.update();

        System.out.print("Size: ");
        Size = inInt.nextInt();

        System.out.print("Color: ");
        Color.delete(0, Color.length());
        Color.append(inStr.nextLine());
    }

    @Override
    public void delete() {
        Size = 0;
        Color.delete(0, Color.length());

        super.delete();
    }
}

