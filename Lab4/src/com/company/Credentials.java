package com.company;

import java.util.Scanner;
import java.util.UUID;

public class Credentials {

    Scanner inStr = new Scanner(System.in);
    Scanner inInt = new Scanner(System.in);

    UUID ID;
    StringBuilder Name = new StringBuilder(); //Имя
    StringBuilder Surname = new StringBuilder();  //Фамилия
    StringBuilder Patronymic = new StringBuilder(); //Отчество
    StringBuilder EMail = new StringBuilder();  //E-Mail

    public Credentials(){
        ID = UUID.randomUUID();
    }

    public Credentials(UUID id){
        ID = id;
    }

    public void create(){
        String[] surnam = {"Иванов", "Петров", "Макаров"};
        String[] nam = {"Иван", "Петр", "Макар"};
        String[] patronym = {"Иванович", "Петрович", "Макарович"};
        String[] emai = {"gmail", "mail", "yandex"};

        Surname.append(surnam[(int)(Math.random()*3)]);
        Name.append(nam[(int)(Math.random()*3)]);
        Patronymic.append(patronym[(int)(Math.random()*3)]);
        EMail.append(emai[(int)(Math.random()*3)]);
    }

    public StringBuilder getName(){
        return Name;
    }

    public StringBuilder getSurname(){
        return Surname;
    }

    public StringBuilder getPatronymic(){
        return Patronymic;
    }

    public StringBuilder getEMail(){
        return EMail;
    }

    public void update(){
        System.out.println("Какую информацию вы хотите изменить?");
        System.out.println("1. Имя");
        System.out.println("2. Фамилия");
        System.out.println("3. Отчество");
        System.out.println("4. E-mail");
        int command = inInt.nextInt();

        switch (command){
            case 1:{
                System.out.println("Введите новое Имя");
                Name.append(inStr.nextLine());
                break;
            }

            case 2:{
                System.out.println("Введите новое Фамилия");
                Surname.append(inStr.nextLine());
                break;
            }

            case 3:{
                System.out.println("Введите новое Отчество");
                Patronymic.append(inStr.nextLine());
                break;
            }

            case 4:{
                System.out.println("Введите новое E-mail");
                EMail.append(inStr.nextLine());
                break;
            }
        }
    }

    public void delete(){
        Name.delete(0, Name.length());
        Surname.delete(0, Surname.length());
        Patronymic.delete(0, Patronymic.length());
        EMail.delete(0, EMail.length());
    }

    public void show() {
        System.out.print("\n\n" + ID + "\n" + Surname + " " + Name + " " + Patronymic + "\n" + EMail + "\n\n");
    }

}
