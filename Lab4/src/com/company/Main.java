//Вариант 3
//Для выхода из программы используется нажатие "q"
package com.company;

import com.check.ACheck;
import com.check.ACheckStatus;
import com.check.Generator;
import com.check.ACheckDelete;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String quit;

        Orders<Order> orders = new Orders<>();
        ACheck generator;
        ACheck checkStatus;
        ACheck checkDelete;

        while (1 > 0) {
            quit = sc.nextLine();
            if ("q".equals(quit))
            {
                break;
            }
            generator = new Generator(orders);
            checkStatus = new ACheckStatus(orders);
            checkDelete = new ACheckDelete(orders);
            try {
                generator.getThread().join();
                checkStatus.getThread().join();
                checkDelete.getThread().join();
            } catch (InterruptedException r) {
                r.printStackTrace();
            }
            orders.show();

            System.out.println("-----------Check List--------");

            int rand = 1 + (int) (Math.random() * 4);
            try {
                TimeUnit.SECONDS.sleep(rand);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}

