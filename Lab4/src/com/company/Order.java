package com.company;

import java.util.Date;

public class Order {

    StringBuilder Status;
    Date TimeOfCreate;
    long TimeOfWait;
    ShoppingCart Cart;
    Credentials man;

    public Order(){
        Status = new StringBuilder("В ожидании");
        TimeOfWait = 2000;
    }

    public Date getTimeOfCreate(){
        return TimeOfCreate;
    }

    public long getTimeOfWait(){
        return TimeOfWait;
    }

    public StringBuilder getStatus(){
        return Status;
    }

    public void changeStatus(){
        Date d = new Date(TimeOfCreate.getTime() + TimeOfWait);
        if (d.equals(new Date()) || new Date().after(d)) {
            Status = new StringBuilder("Обработан");
        }
    }

    public void show(){
        System.out.println("\n");
        man.show();
        System.out.print(Status + " " + TimeOfCreate + " " + TimeOfWait / 60 / 1000 + "\n");
        Cart.show();
    }

    public void checkout(ShoppingCart Cart, Credentials man){
        this.Cart = Cart;
        this.man = man;
        TimeOfCreate = new Date();
    }

    public boolean status(){
        String a = new String("Обработан");
        if (Status.toString().equals(a)){
            return true;
        } else {
            return false;
        }
    }
}
