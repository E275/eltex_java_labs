package com.company;

import java.util.Scanner;
import java.util.UUID;

public abstract class Clothes implements ICrudAction {
    Scanner inStr = new Scanner(System.in);
    Scanner inInt = new Scanner(System.in);

    UUID ID;
    StringBuilder Name = new StringBuilder();
    int Cost;
    int count;
    StringBuilder CompanyName = new StringBuilder();
    static int countObj;
    String[] ColorArray = {"Black", "White", "Blue", "Yellow", "Green"};

    Clothes(){
        countObj++;
        ID = UUID.randomUUID();
    }

    Clothes(UUID _id){
        countObj++;
        ID = _id;
    }

    @Override
    public void create(){
        Name.delete(0, Name.length());
        Name.append("Wood" + countObj);

        Cost = (int)(Math.random() * 100 + 1);
        count = (int)(Math.random() * 25 + 1);

        CompanyName.delete(0, CompanyName.length());
        CompanyName.append("Company_Name " + countObj);
    }

    @Override
    public void read() {
        System.out.println();
        System.out.println("ID:" + ID);
        System.out.println("Name: " + Name);
        System.out.println("Cost: " + Cost);
        System.out.println("Count: " + count);
        System.out.println("Company Name: " + CompanyName);
    }

    @Override
    public void update() {

        System.out.print("Name: ");
        Name.delete(0, Name.length());
        Name.append(inStr.nextLine());

        System.out.print("Cost: ");
        Cost = inInt.nextInt();

        System.out.print("Count: ");
        count = inInt.nextInt();

        System.out.print("Company Name: ");
        CompanyName.delete(0, CompanyName.length());
        CompanyName.append(inStr.nextLine());
    }

    @Override
    public void delete() {
        Name.delete(0, Name.length());
        Cost = 0;
        count = 0;
        CompanyName.delete(0, CompanyName.length());

        countObj--;
    }

    int getCountObj(){
        return countObj;
    }
}
