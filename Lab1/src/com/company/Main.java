package com.company;
//Вариант 3

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner inInt = new Scanner(System.in);
        System.out.println();

        int command;
        int j = 0;
        TShirt[] ArrayTShirt = new TShirt[0];
        TShirt[] TShirtBuf;
        Cap[] ArrayCap = new Cap[0];
        Cap[] CapBuf;
        int countObjTShirt = 0;
        int countObjCap = 0;

        for (int i = 0; i < 2; i++){

            switch (args[j]){
                case "TShirt":{
                    countObjTShirt = Integer.parseInt(args[j + 1]);
                    ArrayTShirt = new TShirt[countObjTShirt];
                    break;
                }

                case "Cap":{
                    countObjCap = Integer.parseInt(args[j + 1]);
                    ArrayCap = new Cap[countObjCap];
                    break;
                }
            }
            j += 2;
        }

        for (j = 0; j < args.length; j += 2) {
            switch (args[j]) {
                case "TShirt": {
                    for (int i = 0; i < Integer.parseInt(args[j + 1]); i++) {
                        ArrayTShirt[i] = new TShirt();
                        ArrayTShirt[i].create();
                    }
                    break;
                }

                case "Cap": {
                    for (int i = 0; i < Integer.parseInt(args[j + 1]); i++) {
                        ArrayCap[i] = new Cap();
                        ArrayCap[i].create();
                    }
                    break;
                }
            }
        }

        do {
            System.out.println("MENU");
            System.out.println("1. Create new object");
            System.out.println("2. Read object");
            System.out.println("3. Update object");
            System.out.println("4. Delete last object");
            System.out.println("0. EXIT");
            command = inInt.nextInt();
            int command2;
            switch (command) {
                case 1:{
                    System.out.println("Choose class");
                    System.out.println("1. T-Shirt");
                    System.out.println("2. Cap");
                    command2 = inInt.nextInt();

                    switch (command2){
                        case 1:{        //create
                            TShirtBuf = new TShirt[countObjTShirt];
                            for (int i = 0; i < countObjTShirt; i++)
                                TShirtBuf[i] = ArrayTShirt[i];

                            countObjTShirt++;
                            ArrayTShirt = new TShirt[countObjTShirt];
                            ArrayTShirt[countObjTShirt - 1] = new TShirt();

                            for (int i = 0; i < countObjTShirt - 1; i++)
                                ArrayTShirt[i] = TShirtBuf[i];

                            ArrayTShirt[countObjTShirt - 1].create();

                            break;
                        }

                        case 2:{
                            CapBuf = new Cap[countObjCap];
                            for (int i = 0; i < countObjCap; i++)
                                CapBuf[i] = ArrayCap[i];

                            countObjCap++;
                            ArrayCap = new Cap[countObjCap];
                            ArrayCap[countObjCap - 1] = new Cap();

                            for (int i = 0; i < countObjCap - 1; i++)
                                ArrayCap[i] = CapBuf[i];

                            ArrayCap[countObjCap - 1].create();

                            break;
                        }

                    }

                    break;
                }

                case 2:{            //read
                    System.out.println("Choose class");
                    System.out.println("1. T-Shirt");
                    System.out.println("2. Cap");
                    command2 = inInt.nextInt();
                    int command3;

                    switch (command2){      //T-Shirt
                        case 1:{

                            System.out.println("Choose operation:");
                            System.out.println("1.All");
                            System.out.println("2.By number");
                            command3 = inInt.nextInt();

                            switch (command3){
                                case 1:{
                                    for (int i = 0; i < countObjTShirt; i++)
                                        ArrayTShirt[i].read();

                                    break;
                                }

                                case 2:{
                                    System.out.println("Enter number ");
                                    int i = inInt.nextInt();
                                    ArrayTShirt[i].read();

                                    break;
                                }


                            }

                            break;
                        }

                        case 2:{            //Cap
                            System.out.println("Choose operation:");
                            System.out.println("1.All");
                            System.out.println("2.By number");
                            command3 = inInt.nextInt();

                            switch (command3){
                                case 1:{
                                    for (int i = 0; i < countObjCap; i++)
                                        ArrayCap[i].read();

                                    break;
                                }

                                case 2:{
                                    System.out.println("Enter number ");
                                    int i = inInt.nextInt();
                                    ArrayCap[i].read();

                                    break;
                                }

                            }

                            break;
                        }
                    }

                    break;
                }

                case 3:{            //update
                    System.out.println("Choose class");
                    System.out.println("1. T-Shirt");
                    System.out.println("2. Cap");
                    command2 = inInt.nextInt();
                    int pos;

                    switch (command2){
                        case 1:{
                            System.out.print("Enter number object(from 0 to " + (countObjTShirt - 1) + "):");
                            pos = inInt.nextInt();
                            ArrayTShirt[pos].update();

                            break;
                        }

                        case 2:{
                            System.out.print("Enter number object(from 0 to " + (countObjCap - 1) + "):");
                            pos = inInt.nextInt();
                            ArrayCap[pos].update();

                            break;
                        }
                    }

                    break;
                }

                case 4:{            //delete
                    System.out.println("Choose class");
                    System.out.println("1. T-Shirt");
                    System.out.println("2. Cap");
                    command2 = inInt.nextInt();

                    switch (command2){
                        case 1:{
                           ArrayTShirt[countObjTShirt - 1].delete();

                            break;
                        }

                        case 2:{
                            ArrayCap[countObjCap - 1].delete();

                            break;
                        }
                    }

                    break;
                }

                case 0:{
                    break;
                }

                default:{
                    System.out.println("No operation");
                }
            }
        } while (command > 0);
    }
}
