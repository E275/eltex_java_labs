package com.company;

import java.util.UUID;

public class Cap extends Clothes {
    StringBuilder Color = new StringBuilder();
    StringBuilder Material = new StringBuilder();
    String[] MaterialArray = {"Cotton", "Polyester", "Cloth"};

    Cap(){
        super();
    }

    Cap(UUID _id){
        super(_id);
    }

    @Override
    public void create() {
        super.create();

        Color.append(ColorArray[(int)(Math.random() * 4)]);

        Material.append(MaterialArray[(int)(Math.random() * 2)]);
    }

    @Override
    public void read() {
        System.out.print("Class: Cap");

        super.read();

        System.out.println("Color: " + Color);
        System.out.println("Material: " + Material);
        System.out.println();
    }

    @Override
    public void update() {
        super.update();

        Color.delete(0, Color.length());
        Color.append(inStr.nextLine());

        Material.delete(0, Material.length());
        Material.append(inStr.nextLine());
    }

    @Override
    public void delete() {
        Color.delete(0, Color.length());
        Material.delete(0, Material.length());

        super.delete();
    }
}
