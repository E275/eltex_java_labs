package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ShoppingCart<T extends Clothes>{

    private List<T> Cart;
    private Set<String> treeSet;

    ShoppingCart(){
        Cart = new ArrayList<T>();
        treeSet = new TreeSet<>();
    }

    public void add(T obj){
        Cart.add(obj);
        treeSet.add(obj.getID());
    }

    public void delete(int count){
        treeSet.remove(Cart.get(count).getID());
        Cart.remove(count);
    }

    public void show(){
        System.out.println("\n");
        if (Cart.isEmpty()) {
            System.out.println("Корзина пуста");
        } else {
            for (int i = 0; i < Cart.size(); i++) {
                Cart.get(i).read();
                System.out.println("\n");
            }
            System.out.println("\n");
        }
    }

    public List get(){
        return Cart;
    }

    public void search (String id){
        if (treeSet.contains(id))
            System.out.println("Товар найден");
        else
            System.out.println("Товар не найден");
    }
}
