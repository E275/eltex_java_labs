//Вариант 3
package com.company;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println();

        String str;
        TShirt[] ArrayTShirt;
        Cap[] ArrayCap;
        int countObjTShirt = 0;
        int countObjCap = 0;
        ShoppingCart Cart = new ShoppingCart();
        Order order = new Order();
        Orders orders = new Orders();
        Credentials man = new Credentials();

        for (int i = 0; i < args.length; i += 2){

            switch (args[i]){
                case "TShirt":{
                    countObjTShirt += Integer.parseInt(args[i + 1]);

                    break;
                }

                case "Cap":{
                    countObjCap += Integer.parseInt(args[i + 1]);

                    break;
                }
            }
        }

        ArrayTShirt = new TShirt[countObjTShirt];

        for (int i = 0; i < countObjTShirt; i++) {
            ArrayTShirt[i] = new TShirt();
            ArrayTShirt[i].create();
        }

        ArrayCap = new Cap[countObjCap];

        for (int i = 0; i < countObjCap; i++) {
            ArrayCap[i] = new Cap();
            ArrayCap[i].create();
        }

        man.create();

        //Добавление в корзину 2 товара: футболка(T-Shirt) и кепка(Cap)
        Cart.add(ArrayTShirt[0]);
        Cart.add(ArrayCap[0]);
        Cart.show();

        //Поиск по ID в корзине
        System.out.println("Введите ID:");
        str = in.nextLine();
        Cart.search(str);

        //Удаление первого товара в корзине
        Cart.delete(0);
        Cart.show();

        //Оформление заказа
        order.checkout(Cart, man);
        orders.add(order);
        Cart = new ShoppingCart();
        order = new Order();
        orders.show();

        //Задержка программы на 2 секунды
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException ex) {}

        orders.show();
        Cart.show();

    }
}
